package tests;

import pages.AddSuppliers;
import pages.LoginFormPage;
import pages.LoginPage;
import navegador.Web;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class CadastroDeUsuarioAccounts {
    private WebDriver navegador;

    @Before
    public void setUp(){
        navegador = Web.openNavigate();
    }

    @Test
    public void testLoginDoUsuario(){
        new LoginFormPage(navegador)
                //.loginPainel()
                .typeEmail("admin@phptravels.com")
                .typePassword("demoadmin")
                .clickLogin()
                .clickMenuAccounts()
                .clickAddSuppliers()
                .FirstName("Wellington")
		        .LastName("Oliveira")
		        .Email("teste@gmail.com")
		        .Password("12345678")
		        .MobileNumber("11-90000-0000")
		        //.Country("American Samoa")
		        .Address1("11-0000-000")
		        .Address2("11-0000-000")
		        .Status("Enabled")
		        //.Newsletter(null)
		        .Applyfor("Hotels")
		        .Name("Tom")
		        //.AssignHotels("Hotels Teste")
		        //.AssignTours("Tours Teste")
		        //.AssignCars("Cars Teste")
        		.clickSalve();
        }

    @After
    public void tearDown(){
        //navegador.quit();
    }
}
