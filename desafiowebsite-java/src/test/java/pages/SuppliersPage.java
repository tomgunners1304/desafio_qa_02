package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SuppliersPage extends DefaultPage{

	public SuppliersPage(WebDriver navegador) {
		super(navegador);
		// TODO Auto-generated constructor stub
	}

	public AddSuppliers clickAddSuppliers() {
		navegador.findElement(By.xpath("/html/body/div[2]/div/div/div/form/button")).click();
		
		return new AddSuppliers(navegador);
	}
}
