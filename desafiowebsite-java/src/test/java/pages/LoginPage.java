package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends DefaultPage{

    public LoginPage(WebDriver navegador) {
		super(navegador);
		// TODO Auto-generated constructor stub
	}

	public LoginFormPage loginPainel(){
        navegador.findElement(By.xpath("/html/body/div/form[1]/div[1]/input[1]")).click();

        return new LoginFormPage(navegador);
    }
}
