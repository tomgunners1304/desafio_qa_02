package pages;

import org.apache.poi.ss.formula.functions.Count;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AddSuppliers extends DefaultPage{

	public AddSuppliers(WebDriver navegador) {
		super(navegador);
		// TODO Auto-generated constructor stub
	}

	public AddSuppliers FirstName(String firstname) {
		navegador.findElement(By.name("fname")).sendKeys(firstname);
		return this;
	}
	
	public AddSuppliers LastName(String lastname) {
		navegador.findElement(By.name("lname")).sendKeys(lastname);
		return this;
	}
	
	public AddSuppliers Email(String email) {
		navegador.findElement(By.name("email")).sendKeys(email);
		return this;
	}
	
	public AddSuppliers Password(String password) {
		navegador.findElement(By.name("password")).sendKeys(password);
		return this;
	}
	
	public AddSuppliers MobileNumber(String mobilenumber) {
		navegador.findElement(By.name("mobile")).sendKeys(mobilenumber);
		return this;
	}
	
	public AddSuppliers Country(String country) {
		WebElement campoType = navegador.findElement(By.id("s2id_autogen1"));
		new Select(campoType).selectByVisibleText(country);
		return this;
	}
	
	public AddSuppliers Address1(String address1) {
		navegador.findElement(By.name("address1")).sendKeys(address1);
		return this;
	}
	
	public AddSuppliers Address2(String address2) {
		navegador.findElement(By.name("address2")).sendKeys(address2);
		return this;
	}
	
	public AddSuppliers Status(String status) {
		WebElement campoType = navegador.findElement(By.name("status"));
		new Select(campoType).selectByVisibleText(status);
		return this;
	}
	
	public AddSuppliers Applyfor(String applyfor) {
		WebElement campoType = navegador.findElement(By.name("applyfor"));
		new Select(campoType).selectByVisibleText(applyfor);
		return this;
	}
	
	public AddSuppliers Name(String name) {
		navegador.findElement(By.name("itemname")).sendKeys(name);
		return this;
	}
	
	public AddSuppliers Newsletter(String newsletter) {
		navegador.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::ins[1]")).click();
		return this;
	}
	
	public AddSuppliers AssignHotels(String assignhotels) {
		navegador.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Assign Hotels'])[1]/following::ul[1]")).sendKeys(assignhotels);
		return this;
	}
	
	public AddSuppliers AssignTours(String assigntours) {
		navegador.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Assign Tours'])[1]/following::ul[1]")).sendKeys(assigntours);
		return this;
	}
	
	public AddSuppliers AssignCars(String assigncars) {
		navegador.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Assign Cars'])[1]/following::ul[1]")).sendKeys(assigncars);
		return this;
	}
	
	public AddSuppliers clickSalve() {
		navegador.findElement(By.xpath("/html/body/div[2]/div/div/form/div/div[3]/button")).click();
		return new AddSuppliers(navegador);
	}
	
	public AddSuppliers addSuppliers(String firstname, String lastname, String email, String password, String mobilenumber, String country, String address1, String address2, String status, String applyfor, String name, String newsletter, String assignhotels, String assigntours, String assigncars) {
		FirstName(firstname);
		LastName(lastname);
		Email(email);
		Password(password);
		MobileNumber(mobilenumber);
		Country(country);
		Address1(address1);
		Address2(address2);
		Status(status);
		Applyfor(applyfor);
		Newsletter(newsletter);
		AssignHotels(assignhotels);
		AssignTours(assigntours);
		AssignCars(assigncars);
		
		
		return new AddSuppliers(navegador);
	}
}
