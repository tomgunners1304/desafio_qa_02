package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginFormPage extends DefaultPage {

    public LoginFormPage(WebDriver navegador) {
		super(navegador);
		// TODO Auto-generated constructor stub
	}

	public LoginFormPage typeEmail(String email){
        navegador.findElement(By.name("email")).sendKeys(email);

        return this;
    }

    public LoginFormPage typePassword(String password){
        navegador.findElement(By.name("password")).sendKeys(password);

        return this;
    }

    public DashboardPage clickLogin(){
        navegador.findElement(By.xpath("/html/body/div/form[1]/button")).click();

        return new DashboardPage(navegador);
    }

    public LoginFormPage fazLogin(String email, String password){
        navegador.findElement(By.name("email")).sendKeys(email);
        navegador.findElement(By.name("password")).sendKeys(password);
        navegador.findElement(By.xpath("/html/body/div/form[1]/button")).click();

        return new LoginFormPage(navegador);
    }
}
