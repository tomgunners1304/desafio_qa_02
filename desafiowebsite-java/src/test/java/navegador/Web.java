package navegador;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Web {

    public static WebDriver openNavigate(){
        System.setProperty("webdriver.chrome.driver", "C:\\JUnit\\desafiowebsite-java\\chromedriver.exe");
        WebDriver navegador = new ChromeDriver();
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        navegador.get("https://www.phptravels.net/admin");

        return navegador;
    }
}
